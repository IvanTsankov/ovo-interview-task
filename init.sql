CREATE TABLE advices (
    id SERIAL PRIMARY KEY,
    user_number INTEGER NOT NULL,
    record_type VARCHAR,
    effective_date DATE,
    reference VARCHAR,
    payer_name VARCHAR,
    JOHN VARCHAR,
    payer_account_number INTEGER,
    payer_sort_code VARCHAR,
    reason_code VARCHAR,
    aosn VARCHAR,
    transaction_code VARCHAR,
    orig_sort_code VARCHAR,
    orig_account_number INTEGER,
    original_proc_date DATE,
    originator_name VARCHAR
);

CREATE UNIQUE INDEX advices_user_number ON advices (user_number);