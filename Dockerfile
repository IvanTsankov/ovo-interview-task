FROM node:latest

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install -g serverless
RUN npm install -g serverless-offline
RUN npm install

COPY . .

EXPOSE 3300

CMD [ "sls", "offline", "start" ]