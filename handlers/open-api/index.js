const openApi = require("../../lib/open-api/open-api");

module.exports.handler = async (event) => {
    try {
        const yml = await openApi();

        return {
            statusCode: 200,
            body: JSON.stringify(yml),
        };
    } catch (error) {

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: error
            })
        }
        
    }
};
