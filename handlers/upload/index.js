const upload = require("../../lib/upload/upload");

module.exports.handler = async (event) => {
    try {
        const body = JSON.parse(event.body);

        await upload(body);

        return {
            statusCode: 200,
            body: JSON.stringify({
                success: true
            })
        };
    } catch (error) {

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: error.message
            })
        }
    }
}