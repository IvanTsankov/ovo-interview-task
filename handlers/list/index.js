const list = require("../../lib/list/list");

module.exports.handler = async (event) => {
    try {
        const { id } = event.pathParameters;
        const { to, from } = event.multiValueQueryStringParameters || {};
        const result = await list(id, to, from);
        

        return {
            statusCode: 200,
            body: JSON.stringify(result.rows)
        };
    } catch (error) {
        console.log(error)
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: error
            })
        }
        
    }
};
