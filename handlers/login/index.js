const login = require("../../lib/login/login")

module.exports.handler = async (event) => {
    try {
        const { id } = event.pathParameters;
        const token = await login(id);

        return {
            statusCode: 200,
            body: JSON.stringify({
                token
            })
        }
    } catch (error) {

        return {
            statusCode: 500,
            body: JSON.stringify(error)
        }
    }
}