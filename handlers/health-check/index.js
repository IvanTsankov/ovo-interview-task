const healtCheck = require("../../lib/health-check/health-check.js");

module.exports.handler = async (event) => {
    try {
        await healtCheck();

        return {
            statusCode: 200,
            body: JSON.stringify({
                success: true
            })
        };
    } catch (error) {

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: "Error while checking db health"
            })
        }
    }
}