const yaml = require('js-yaml');
const fs = require('fs');
const path = require("path");

const openApi = async () => {
    const dir = path.resolve(__dirname, '../../openapi.yaml');
    const doc = await fs.readFileSync(dir, 'utf8');
    const data = yaml.safeLoad(doc);

    return data;
}

module.exports = openApi;