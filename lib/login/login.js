const jwt = require('jsonwebtoken');

const login = (id) => {
    return jwt.sign({ id: id }, process.env.JWT_SECRET, {
      expiresIn: 86400
    });
}

module.exports = login;