var convert = require('xml-js');
const fs = require('fs');
const path = require("path");
const { v4: uuidv4 } = require('uuid');
const dbConnect = require("./../db/db")

const upload = async ({
  userNumber,
  effectiveDate,
  origSortCode,
  originatorName,
  recordType,
  reference,
  payerName,
  payerAccountNumber,
  payerSortCode,
  reasonCode,
  aosn,
  transactionCode,
  origAccountNumber,
  originalProcDate,
}) => {

  const client = await dbConnect();
  const query = {
    name: 'fetch-user-advices',
    text: `INSERT INTO advices (user_number, record_type, effective_date, reference, payer_name, payer_account_number, payer_sort_code, reason_code, aosn, transaction_code, orig_sort_code, orig_account_number, original_proc_date, originator_name) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)`,
    values: [
      userNumber,
      recordType,
      effectiveDate,
      reference,
      payerName,
      payerAccountNumber,
      payerSortCode,
      reasonCode,
      aosn,
      transactionCode,
      origSortCode,
      origAccountNumber,
      originalProcDate,
      originatorName,
    ]
  };

  const result = await client.query(query);


  const dir = path.resolve(__dirname, '../../files/123456_WEIRDREPORTTYPE_20200711012345.XML');
  const xml = await fs.readFileSync(dir, 'utf8');
  const data = JSON.parse(convert.xml2json(xml, {compact: true, spaces: 4}));
  
  if (data) {
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["record-type"] = recordType;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["effective-date"] = effectiveDate;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["reference"] = reference;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["payer-name"] = payerName;
    data.BACSDocument.Data.MessagingAdvices.MessagingHeader._attributes["user-number"] = userNumber;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["payer-account-number"] = payerAccountNumber;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["payer-sort-code"] = payerSortCode;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["reason-code"] = reasonCode;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice.aosn = aosn;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["transaction-code"] = transactionCode;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["orig-sort-code"] = origSortCode;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["orig-account-number"] = origAccountNumber;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["original-proc-date"] = originalProcDate;
    data.BACSDocument.Data.MessagingAdvices.MessagingAdvice["originator-name"] = originatorName;

  }

  const newXML = convert.json2xml(JSON.stringify(data), {compact: true, ignoreComment: false, spaces: 2});
  const date = new Date();
  const fileName = `${uuidv4()}-WEIRDREPORTTYPE-${date.getMilliseconds()}.XML`
  await fs.writeFile(path.resolve(__dirname, `../../files/${fileName}`), newXML, (err) => {
    if (err) throw err;
  });

  
  return result.rows[0];
};

module.exports = upload;