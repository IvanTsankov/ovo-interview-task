const { Client } = require('pg');

const dbConnect = async () => {
    const client = new Client({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        port: process.env.DB_PORT,
    });
    
    await client.connect();
    return client;
}

module.exports = dbConnect;