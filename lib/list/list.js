const dbConnect = require("../db/db");

const list = async (id, to, from) => {
    const client = await dbConnect();
    const query = {
        name: 'fetch-user-advices',
        text: `SELECT * FROM advices WHERE id = '${id}'`,
      };
    if (from) {
      query.text += `AND effective_date >= '${from}'`
    }
    if (to) {
      query.text += `AND effective_date <= '${to}'`
    }
    return await client.query(query);
}

module.exports = list;